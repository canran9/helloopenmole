name := "deploiement"

version := "0.1"

scalaVersion := "2.13.1"

mainClass := Some("Index")

libraryDependencies += "com.lihaoyi" %% "scalatags" % "0.8.2"

libraryDependencies += "com.github.pathikrit" %% "better-files" % "3.8.0"
